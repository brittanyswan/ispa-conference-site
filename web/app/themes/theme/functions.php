<?php
	
	// Add RSS links to head section
	add_theme_support('automatic-feed-links');

	// Remove Emoji JS
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');

	// Clean up the head
	function removeHeadLinks(){
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		wp_deregister_script('comment-reply');
	 }
	 add_action('init', 'removeHeadLinks');
	 remove_action('wp_head', 'wp_generator');

	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => 'Sidebar Widgets',
			'id'   => 'sidebar-widgets',
			'description'   => 'These are widgets for the sidebar.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));
	}

	// Hide admin bar on front end
	add_filter('show_admin_bar', '__return_false');
	
	// Add post thumbnails support 
	add_theme_support('post-thumbnails', array('post', 'page')); 

	// Load Stylesheets and Scripts
	function startTheme(){
		wp_enqueue_style('theme-style', get_template_directory_uri() . '/css/main.css');
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"), '', '1.11.3', true);
		wp_enqueue_script('jquery');
		wp_register_script('slick-script', get_template_directory_uri() . '/js/slick.min.js', '', '1.11.3', true);
		wp_enqueue_script('slick-script');
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/main.min.js', '', '1.0', true );
	}
	add_action('wp_enqueue_scripts', 'startTheme');

?>